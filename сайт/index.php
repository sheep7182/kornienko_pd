
<!--header open-->
<?php include("header.php") ?>

	<body>

<main role="main">

  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="img/base/1.png" width="100%" height="100%">
        <div class="container">
          <div class="carousel-caption text-left">
            <h1>Что такое Lightning Network?</h1>
            <p>Lightning Network – это механизм, созданный для быстрого проведении транзакций.<br>
							Lightning Network работает с помощью каналов, скорость которых не зависит от текущей загруженности блоков.</p>
            <p><a class="btn btn-lg btn-primary"  href="lightning.php">Узнать больше</a></p>
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <img src="img/base/2.jpg" width="100%" height="100%">
        <div class="container">
          <div class="carousel-caption">
            <p><a class="btn btn-lg btn-primary" href="wallet.php">Узнать больше</a></p>
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <img src="img/base/3.jpg" width="100%" height="100%">
        <div class="container">
          <div class="carousel-caption text-right">
            <p><a class="btn btn-lg btn-primary" href="service.php">Узнать больше</a></p>
          </div>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>


  <div class="container marketing">

    <h1 align="center">Проектная деятельность 3 семестр</h1>
    <div class="row">
      <div class="col-lg-4">
        <img src="img/base/6.jpg" width="200" height="140">
        <h2><a href="http://project-activity-2019-spring.std-001.ist.mospolytech.ru/">Тема проекта, задание</a></h2>
        <h4 class="lead">«Изучение и анализ возможностей применения технологий Lightning Network»:поработать с Lightning кошельком для IOS. Составить обзорный отчет на
					технологию Lightning Network и исследуемый кошелёк.</h4>
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-4">
        <img src="img/base/7.jpg" width="200" height="140">
        <h2> Список участников, этапы работы над проектом</h2>
        <h4 class="lead">Корниенко Екатерина Максимовна группа 181-331: сбор и анализ информации, составление отчёта в форме доклада, оформление отчёта в виде сайта</h4>
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-4">
        <img src="img/base/8.jpg" width="200" height="140">
				<h2><a href="https://gitlab.com/sheep7182/kornienko_pd">GitLab</a></h2>
        <p>Ход работы над проектом</p>
      </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->


    <!-- START THE FEATURETTES -->
	<hr class="featurette-divider">
    <div class="row featurette">
      <div class="col-md-7 order-md-2">
        <h2 >Заключение об исследуемом кошельке</h2>
        <p class="lead">Кошелёк удобен в использовании, кроме того, в политике конфиденциальности делается акцент на пунктах о сохранности личных данных пользователя. При использовании
				кошелька не нужно вводить никаких персональных данных (почты,данные кредитных карт, паспортные данные), что позволяет не беспокоиться об их краже или о неправильном заполнении полей. </p>
      </div>
      <div class="col-md-5 order-md-1">
        <img src="img/base/5.png" width="400" height="400">
      </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
      <div class="col-md-7">
        <h2>Заключение об LN</h2>
        <p class="lead">Таким образом, Lightning Network – это технология будущего, она предлагает повышенные меры безопасности, что очень важно при работе
					 с финансами, при этом не теряя в скорости.  Любому человеку в IT важно понимать, какие прорывы происходят в
					  его сфере, особенно, если новое решение стремительно в своём развитии и с каждым годом приобретает всё большую популярность. </p>
      </div>
      <div class="col-md-5">
        <img src="img/base/4.jpg" width="400" height="400">
      </div>
    </div>

    <hr class="featurette-divider">

    <!-- /END THE FEATURETTES -->

  </div><!-- /.container -->
<?php include ("footer.php") ?>
