<html lang="ru">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="/css/style.css">
   <title>Kornienko</title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="http://pd.std-821.ist.mospolytech.ru/">На главную</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-item nav-link" href="lightning.php">Lightning Network</a>
        <a class="nav-item nav-link" href="wallet.php">Кошелёк</a>
        <a class="nav-item nav-link" href="service.php">Приложение</a>
        <a class="nav-item nav-link" href="https://gitlab.com/sheep7182/kornienko_pd">GitLab</a>
      </div>
    </div>
  </nav>
