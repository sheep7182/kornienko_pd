<!DOCTYPE html>
<?php include("header.php") ?>

<div class="dark-section">
    <div class="container site-section">
        <h1>Приложение</h1>
        <div class="row">
            <div class="col-md-12 item">
                <h2>Возможности пополненного кошелька</h2>
                <p>После пополнения счета появляется новая кнопка "<a href="https://bluewallet.io/marketplace/">marketplace</a>" слева под оранжевой карточкой кошелька.
                   Это каталог сервисов, в которых можно оплатить с помощью технологии Lightning Network.</a></p>
                   <img src="img/service/1.jpg" width="350" height="400">
                   <img src="img/service/2.jpg" width="350" height="400">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 item">
                <h2>Выбранное приложение</h2>
                <p>В разделе “Games” выбираю “<a href="https://agar.satoshis.games/?utm_source=bluewallet">Agar</a>”. Agar- это многопользовательская онлайн-игра на Lightning Network, выпущенная компанией Satoshi's Games.
                  Так как это приложение создано полностью на LN, в нём невозможно обойтись без этой технологии.
                   Данное приложение является хорошим развлечением, с помощью которого можно заработать сатоши, при том, что взнос для начала игры равен 1000 сатошам (~5рублей).
                   После нажатия кнопки "Play" появляется QR-код для оплаты игры, так как кошелёк автоматически привязывается к приложению (в случае перехода из приложения Blue Wallet), игру можно оплатить, нажав кнопку "Confirm".
                    Фактически, сама возможность совершения реальных микротранзакций между игроками является огромным прорывом, так как ранее существовавшие технологии были слишком дорогими или медленными для совершения таких операций.</a></p>
                   <img src="img/service/3.jpg" width="350" height="400">
                   <img src="img/service/4.jpg" width="350" height="400">
            </div>
        </div>
    </div>
</div>
<?php include ("footer.php") ?>
